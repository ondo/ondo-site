class CreateBlogComments < ActiveRecord::Migration
  def change
    create_table :blog_comments do |t|
      t.string :email
      t.text :body
      t.integer :blog_id

      t.timestamps
    end
  end
end
