class AddImageToBlog < ActiveRecord::Migration
  def up
  	add_column :blogs, :image, :string
  end

  def down
  	add_column :blogs, :image, :string
  end
end
