class AddNameToBlogComments < ActiveRecord::Migration
  def change
    add_column :blog_comments, :commenter_name, :string
    add_column :blog_comments, :commenter_website, :string
  end
end
