ActiveAdmin.register Blog do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form do |f|
    input :title
    input :body
    input :image
    input :tag_list, :placeholder => "rails, tech, etc"
    actions
  end

  permit_params :title, :body, :image, :tag_list => []

end
