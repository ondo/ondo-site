class WelcomeController < ApplicationController
  def index
    # whatever your controller needs to do...
    
    render file: "#{Rails.root}/public/index.html.erb"
  end
end
